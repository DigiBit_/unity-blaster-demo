﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using DigiBitSDK;

public class PluginSceneManager : MonoBehaviour {

	//edit this script as necessary but keep the IsActive bool there to check for multiple instance of scene
	//Led pattern requires new firmware
	//game should be paused when digibits disconnect as this script does not handle game pause

	static bool IsActive = false; //to prevent multiple instances of scene to load
	//static bool FirstLoad = true;

	public GameObject FlashingPanel;
	public GameObject OlderFirmwarePanel;
	public GameObject WaitingForConnectionPanel;
	public GameObject WaitingForGameToStartPanel;
	public Text GameResumeCounter;


	public bool PlayWithoutDigiBits = false;
	public bool WaitBeforeResuming = true;

	Scene CurrentScene;
	bool EndScene;
	//int RequiredFirmwareVersion = 4;

	void Awake()
	{
		if (IsActive) {
			SceneManager.UnloadSceneAsync ("PluginStartUp");
			return;
		}

		IsActive = true;
	}

	void Start () {

		FlashingPanel.SetActive (false);
		OlderFirmwarePanel.SetActive (false);
		WaitingForConnectionPanel.SetActive (true);

		CurrentScene = SceneManager.GetActiveScene();

		ManageCameraPosition ();
		ManageEventSystem ();
		ManageSortingOrder ();
	}



	void Update ()
	{
		if (EndScene)
			return;
		if (DigiBit.PrimaryConnected && DigiBit.SecondaryConnected || PlayWithoutDigiBits) {
			WaitingForConnectionPanel.SetActive (false);
//			if ((DigiBit.PrimaryData.FirmwareVersion >= RequiredFirmwareVersion &&
//			    DigiBit.SecondaryData.FirmwareVersion >= RequiredFirmwareVersion) || PlayWithoutDigiBits) {	
//				FlashingPanel.SetActive (true);
//				OlderFirmwarePanel.SetActive (false);
//			} else if (DigiBit.PrimaryData.FirmwareVersion <= 0 || DigiBit.SecondaryData.FirmwareVersion <= 0) {
//				WaitingForConnectionPanel.SetActive (true);
//				FlashingPanel.SetActive (false);
//				OlderFirmwarePanel.SetActive (false);
//			}else {
//				FlashingPanel.SetActive (false);
//				OlderFirmwarePanel.SetActive (true);
//			}

			//for blaster using this directly as there is no way to identify the connected hardware
			ContinueGame ();
				
		} else {
			WaitingForConnectionPanel.SetActive (true);
			FlashingPanel.SetActive (false);
			OlderFirmwarePanel.SetActive (false);
		}
	}


	void ManageCameraPosition()
	{
		float MaxZposition = 0.0f;
		if (CurrentScene.name != "PluginStartUp") {
			foreach (GameObject GameObj in CurrentScene.GetRootGameObjects()) {

				foreach (Transform T in GameObj.GetComponentsInChildren<Transform> (true)) {
					if (MaxZposition < T.position.z)
						MaxZposition = T.position.z;
				}
			}
		}

		Transform CameraTransform = gameObject.GetComponentInChildren<Camera> ().gameObject.transform;
		CameraTransform.position = new Vector3 (CameraTransform.position.x, CameraTransform.position.y, MaxZposition+1.0f);

	}

	void ManageEventSystem()
	{
		if (GameObject.FindObjectOfType<EventSystem> () == null)
			gameObject.GetComponentInChildren<EventSystem>(true).gameObject.SetActive(true);
	}

	void ManageSortingOrder()
	{
		int SortOrder = 1000; //need better way to find this in scene
		string SortingLayerName = SortingLayer.layers [SortingLayer.layers.Length - 1].name;
		FlashingPanel.GetComponent<Canvas> ().sortingLayerName = SortingLayerName;
		FlashingPanel.GetComponent<Canvas> ().sortingOrder= SortOrder;
		WaitingForConnectionPanel.GetComponent<Canvas> ().sortingLayerName = SortingLayerName;
		WaitingForConnectionPanel.GetComponent<Canvas> ().sortingOrder= SortOrder;
	}
		
	
	#region ColorButtonCallbacks
	public void IdentifyWithRed()
	{
		DigiBit.DigiBitIdentify (LEDColor.RED);
	}

	public void IdentifyWithGreen()
	{
		DigiBit.DigiBitIdentify (LEDColor.GREEN);
	}

	public void IdentifyWithBlue()
	{
		DigiBit.DigiBitIdentify (LEDColor.BLUE);
	}

	public void IdentifyWithRWhite()
	{
		DigiBit.DigiBitIdentify (LEDColor.WHITE);
	}
	#endregion

	public void ContinueGame()
	{
//		EndScene = true;
//		if (WaitBeforeResuming && !FirstLoad)
//			StartCoroutine (ResumeGameWithDelay ());
//		else {
//			SceneManager.UnloadSceneAsync("PluginStartUp");
//			IsActive = false;
//			FirstLoad = false;
//		}
//		StartCoroutine (LoadDevice(CurrentVRDevice));
		EndScene = true;
		SceneManager.UnloadSceneAsync("PluginStartUp");
	}

	IEnumerator ResumeGameWithDelay(){

		WaitingForGameToStartPanel.SetActive (true);
		WaitingForConnectionPanel.SetActive (false);
		FlashingPanel.SetActive (false);

		GameResumeCounter.GetComponent<Text> ().text = "Resuming Game in 3";
		yield return new WaitForSecondsRealtime (1.0f);
		GameResumeCounter.GetComponent<Text> ().text = "Resuming Game in 2";
		yield return new WaitForSecondsRealtime (1.0f);
		GameResumeCounter.GetComponent<Text> ().text = "Resuming Game in 1";
		yield return new WaitForSecondsRealtime (1.0f);


		SceneManager.UnloadSceneAsync("PluginStartUp");
		IsActive = false;
		//FirstLoad = false;
	}
}
