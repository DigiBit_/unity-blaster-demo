using UnityEngine;
using System.Collections;
using DigiBitSDK;

public class DigiBitManager : MonoBehaviour
{
     //Tag for Debugging 
     const string TAG = "Digibit-Demo-Unity ";

     void Start()
     {
          Screen.sleepTimeout = SleepTimeout.NeverSleep;

          DigiBitCallBacks.OnDeviceDisconnected += OnDeviceDisconnected;
          DigiBitCallBacks.OnDeviceConnected += OnDeviceConnected;

          //Initializes DigiBit
          DigiBit.Init(true, true, PredefinedPid.Blaster);
     }

     void Update()
     {

     }

     void OnDeviceDisconnected(Devices Device)
     {
          Debug.Log(TAG + "onDeviceDisconnected::" + UtilityFunctions.DecodeToString(Device));
     }

     void OnDeviceConnected(Devices Device)
     {
          Debug.Log(TAG + "OnDeviceConnected::" + UtilityFunctions.DecodeToString(Device));
     }
}
