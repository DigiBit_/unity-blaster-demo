﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;
using DigiBitSDK;

public class GameManager : MonoBehaviour
{

     public Text ConnectionStatus;
     public Text EulerX;
     public Text EulerY;
     public Text EulerZ;
     public Text ButtonUp;
     public Text ButtonDown;
     public Text ButtonLeft;
     public Text ButtonRight;
     public Text ButtonJoystick;
     public Text ButtonTrigger;
     public Text JoystickHorizontal;
     public Text JoystickVertical;

     void Start()
     {


     }

     void Update()
     {
          //keeps connection status updated	
          ConnectionStatus.text = "Blaster Connected: " + DigiBitSDK.DigiBit.PrimaryConnected;

          EulerX.text = "X = " + DigiBit.PrimaryData.EulerAngle.x.ToString("F") + "°";
          EulerY.text = "Y = " + DigiBit.PrimaryData.EulerAngle.y.ToString("F") + "°";
          EulerZ.text = "Z = " + DigiBit.PrimaryData.EulerAngle.z.ToString("F") + "°";

          ButtonUp.text = "Button Up = " + DigiBit.PrimaryData.ButtonUp;
          ButtonDown.text = "Button Down = " + DigiBit.PrimaryData.ButtonDown;
          ButtonLeft.text = "Button Left = " + DigiBit.PrimaryData.ButtonLeft;
          ButtonRight.text = "Button Right = " + DigiBit.PrimaryData.ButtonRight;
          ButtonJoystick.text = "Button Joystick = " + DigiBit.PrimaryData.ButtonJoystick;
          ButtonTrigger.text = "Button Trigger = " + DigiBit.PrimaryData.ButtonTrigger;
          JoystickHorizontal.text = "Joystick Horizontal = " + DigiBit.PrimaryData.JoyStickHorizontal;
          JoystickVertical.text = "Joystick Vertical = " + DigiBit.PrimaryData.JoyStickVertical;
     }
}
